#include <stdio.h>
#include <stdlib.h>
#include "cola.h"

cola *crear_cola(){
	cola *nueva_cola = malloc(sizeof(cola));
	if(nueva_cola != NULL){ //SI LA COLA NO ESTA VACIA ....
		//printf("SI CUMPLIO1");
		nueva_cola -> tamano = 0;
		nueva_cola -> inicio = NULL;
		nueva_cola -> fin = NULL;
		return nueva_cola;
	}else{
		//NO ENTRO
		return NULL;
	}
}

int encolar(cola *mi_cola, void *elm){
	//TENGO QUE VER SI EL INICIO Y FIN APUNTAN A NULL
	if(mi_cola -> inicio == NULL && mi_cola -> fin == NULL){
		mi_cola -> tamano++; //AUMENTA 1 EN TAMANO
		nodo_cola *nodo = malloc(sizeof(nodo_cola));
		nodo -> elemento = malloc(sizeof(int));
		*((int *)nodo -> elemento) = *(int *)elm;
		nodo -> anterior = NULL;
		nodo -> siguiente = NULL;

		mi_cola -> inicio = nodo;
		mi_cola -> fin = nodo;
		mi_cola -> tamano = 1;
		
		return 0;
		//printf("si");
	}
	else{
		//printf("no");
		return 0;
	}
	return -1;
}



void *decolar(cola *mi_cola){
	if(mi_cola -> tamano > 0){ //CREO NODO NUEVO SI CUMPLE,DEVUELVO COLA
		//printf("SI CUMPLIO2");
		mi_cola -> tamano--;
		void *elem = (mi_cola -> inicio) -> elemento;
		nodo_cola *nodC = (mi_cola -> inicio);
		if(mi_cola -> tamano != 0){
			//printf("DENTRO DEL 2);
			(mi_cola -> inicio) = (mi_cola -> inicio) -> siguiente;
			(mi_cola -> inicio)/*->siguiente)*/ ->anterior = NULL;
		}
		//LIBERAR MEMORIA
		free(elem);
		free(nodC);
		return (mi_cola -> inicio) -> elemento;
	}else{
		//NO ENTRO
		return NULL;
	}
}


unsigned long tamano_cola(cola *mi_cola){
return mi_cola -> tamano;
}

unsigned long posicion_cola(cola *mi_cola, void *elm){
	nodo_cola *nodo = mi_cola -> inicio;
	int elem = *(int *)elm;
	for(unsigned long a=0; a < mi_cola -> tamano; a++){
		//printf("si");
		int dato= *(int *)nodo -> elemento;

		if(elem == dato){
			printf("Esta en la posicion: %ld\n",a);
			return a;
		}
		nodo = nodo -> siguiente;
	}
	return -1;
}

int destruir_cola(cola *mi_cola){
	if(mi_cola!=NULL){
		//printf("si");
		free(mi_cola);
		return 0;
	}
	return -1;
}

