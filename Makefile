principal: principal.o util_cola.o
	gcc -Wall principal.o util_cola.o -fsanitize=address,undefined -o principal
util_cola.o: util_cola.c
	gcc -Wall -c util_cola.c -o util_cola.o

principal.o: principal.c
	gcc -Wall -c principal.c -o principal.o


clean:
	rm util_cola.o principal.o principal


